<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-rpx
 * @author     Ruslan Kabalin <ruslan.kabalin@luns.net.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2010 Lancaster University Network Services Limited
 *                      http://www.luns.net.uk
 *
 */

defined('INTERNAL') || die();


$string['title'] = 'RPX';
$string['loginlinktitle'] = 'Sign in using Social Network account';
$string['description'] = 'Authenticate against a Janrain Engage service';
$string['rpxapikey'] = 'The API Key for this site';
$string['updateuserinfoonlogin'] = 'Update user details on login';
$string['widgetstyle'] = 'Widget style';
$string['widgetstyleoverlay'] = 'Modal overlay';
$string['widgetstyleembedded'] = 'Embedded';
$string['notusable'] = 'Plugin is already in use by another institution';
$string['adminurltitle'] = 'Links on Janrain website';
$string['adminurlcontent'] = '<a href="%s" target="_BLANK">Your dashboard</a>';
$string['setupurlcontent'] = '<a href="%s" target="_BLANK">Setup providers</a>';
$string['setupurldescr'] = '(links open in new window)';

$string['erralreadyinuse'] = 'Plugin is already in use by institution "%s"';
$string['errcurl'] = 'Cannot connect to RPX Server, check Site Proxy settings.';
$string['errrpxapikey'] = 'No such key exists on RPX server, make sure that you specified a correct key.';
$string['errnoauthinstance'] = 'PRX plugin is not used by any institution. Please add RPX plugin on institution configuration page.';
$string['errrpx'] = 'PRX irrecoverable error occured. Error code %s: %s';
$string['errnorpxuser'] = 'No User found';
?>