<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-rpx
 * @author     Ruslan Kabalin <ruslan.kabalin@luns.net.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2010 Lancaster University Network Services Limited
 *                      http://www.luns.net.uk
 *
 * This file incorporates work covered by the following copyright and
 * permission notice:
 *
 *    @package    mahara
 *    @subpackage auth-xmlrpc
 *    @author     Catalyst IT Ltd
 *    @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 *    @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details:
 *
 *             http://www.gnu.org/copyleft/gpl.html
 */

defined('INTERNAL') || die();
require_once(get_config('docroot') . 'auth/lib.php');
require_once(get_config('docroot') . 'auth/rpx/lib/rpx.php');

/**
 * Authenticates users using Janrain Engage (former RPX)
 */
class AuthRpx extends Auth {

    public function __construct($id = null) {
        $this->type = 'rpx';
        $this->has_instance_config = true;

        $this->config['rpxapikey'] = '';
        $this->config['updateuserinfoonlogin'] = 1;
        $this->config['rpxadminurl'] = '';
        $this->config['rpxrealm'] = '';
        $this->config['rpxrealmscheme'] = '';
        $this->config['widgetstyle'] = 'overlay';
        $this->instanceid = $id;

        if (!empty($id)) {
            return $this->init($id);
        }
        return true;
    }

    public function init($id = null) {
        $this->ready = parent::init($id);
        return $this->ready;
    }

    public function __get($name) {
	if (array_key_exists($name, $this->config)) {
	    return $this->config[$name];
	}
	if (isset($this->$name)) {
	    return $this->$name;
	}
    }

    public function can_auto_create_users() {
        return true;
    }

    /*
     * Authorisation processing:
     *
     * @param  string RPX token
     * @return bool   true
     */
    public function request_user_authorise($token) {
        global $USER, $SESSION;
        $this->must_be_ready();
        $rpxdata = new RPX;

        // Fetch user data from RPX server
        $rpxdata = $rpxdata->auth_info($token, $this->rpxapikey);

        //log_debug($rpxdata);

        if ($rpxdata['stat'] !== 'ok') {
            // Error occured during api call
            throw new RemoteServerException(get_string('errrpx', 'auth.rpx', $rpxdata['err']['code'], $rpxdata['err']['msg']));
        }

        $create = false;
        $update = false;
        if ($this->config['updateuserinfoonlogin'] == '1') {
            $update = true;
        }

        // See if we already have this user, for this plugin we do not permit
        // 'usersuniquebyusername', so we look for remote user table only.
        try {
            $user = new User;
            $user->find_by_instanceid_username($this->instanceid, $rpxdata['profile']['identifier'], true);

            if ($user->get('suspendedcusr')) {
                die_info(get_string('accountsuspended', 'mahara', strftime(get_string('strftimedaydate'), $user->get('suspendedctime')), $user->get('suspendedreason')));
            }
        }
        catch (AuthUnknownUserException $e) {
            $institution = new Institution($this->institution);
            if ($institution->isFull()) {
                throw new XmlrpcClientException('RPX login attempt failed - institution is full');
            }
            $user = new User;
            $create = true;
        }

        if ($create) {

            $user->passwordchange = 0;
            $user->active         = 1;
            $user->deleted        = 0;

            $user->expiry         = null;
            $user->expirymailsent = 0;
            $user->lastlogin      = time();

            // import required fields
            $imported = $this->import_required_fields($user, $rpxdata);

            $user->authinstance   = $this->instanceid;

            db_begin();

            // username setting
            if (!empty($rpxdata['profile']['preferredUsername'])) {
                $user->username = get_new_username($rpxdata['profile']['preferredUsername']);
            }
            else {
                $user->username = get_new_username(preg_replace('/\s+/', '', $rpxdata['profile']['displayName']));
            }

            $user->id = create_user($user, array(), $this->institution, $this, $rpxdata['profile']['identifier']);

            $locked = $this->import_user_settings($user, $rpxdata);
            $locked = array_merge($imported, $locked);

            /*
             * We need to convert the object to a stdclass with its own
             * custom method because it uses overloaders in its implementation
             * and its properties wouldn't be visible to a simple cast operation
             * like (array)$user
             */
            $userobj = $user->to_stdclass();
            $userarray = (array)$userobj;
            db_commit();

            // Now we have fired the create event, we need to re-get the data
            // for this user
            $user = new User;
            $user->find_by_id($userobj->id);
        }
        elseif ($update) {
            $remoteuser = new stdClass();

            $imported = $this->import_required_fields($remoteuser, $rpxdata);

            foreach ($imported as $field) {
                if ($user->$field != $remoteuser->$field) {
                    $user->$field = $remoteuser->$field;
                    set_profile_field($user->id, $field, $user->$field);
                }
            }
            // import required fields
            $locked = $this->import_user_settings($user, $rpxdata);
            $locked = array_merge($imported, $locked);

            $user->lastlastlogin = $user->lastlogin;
            $user->lastlogin     = time();

            $user->commit();
        }

        // See if we need to create/update a profile Icon image
        if (($create || $update) && !empty($rpxdata['profile']['photo'])) {
            $result = $this->import_profile_icon($user, $rpxdata['profile']['photo'], $create, $update);
            if ($result && $update) {
                $locked[] = 'profileicon';
            }
        }

        // Bring user back to life.
        $USER->reanimate($user->id, $this->instanceid);

        $SESSION->set('authinstance', $this->instanceid);

        if ($update && isset($locked)) {
            $SESSION->set('lockedfields', $locked);
        }

        return true;
    }

    // ensure that a user is logged out of mahara
    public function logout() {
        global $USER, $SESSION;

        // logout of mahara
        $USER->logout();
        $SESSION->set('lockedfields', null);
        redirect(get_config('wwwroot'));
    }

    /**
     * Fetch image by URL and apply it to user profile.
     *
     * @param User  $user
     * @param string  $url image url
     * @param bool    $create if we import profile image for new user
     * @param bool    $update if profile image should be updated
     * @return bool   true on success
     */
    private function import_profile_icon($user, $url, $create, $update) {
        $error ='';

        // fetch image by url
        $result = mahara_http_request(array(CURLOPT_URL => $url));
        $error = $result->error;

        $u = preg_replace('/[^A-Za-z0-9 ]/', '', $user->username);
        $filename = get_config('dataroot') . 'temp/mpi_' . intval($this->instanceid) . '_' . $u;

        if (empty($error) && !empty($result->data)) {
            if (file_put_contents($filename, $result->data)) {
                $imageexists = false;
                $icons = false;

                if ($update) {
                    $newchecksum = sha1_file($filename);
                    $icons = get_records_select_array('artefact', 'artefacttype = \'profileicon\' AND owner = ? ', array($user->id), '', 'id');
                    if (false != $icons) {
                        foreach ($icons as $icon) {
                            $iconfile = get_config('dataroot') . 'artefact/file/profileicons/originals/' . ($icon->id % 256) . '/' . $icon->id;
                            $checksum = sha1_file($iconfile);
                            if ($newchecksum == $checksum) {
                                $imageexists = true;
                                unlink($filename);
                                break;
                            }
                        }
                    }
                }

                if (false == $imageexists) {
                    $filesize = filesize($filename);
                    if (!$user->quota_allowed($filesize)) {
                        $error = get_string('profileiconuploadexceedsquota', 'artefact.file', get_config('wwwroot'));
                    }

                    require_once('file.php');
                    $imagesize = getimagesize($filename);
                    if (!$imagesize || !is_image_type($imagesize[2])) {
                        $error = get_string('filenotimage');
                    }

                    $mime = $imagesize['mime'];
                    $width = $imagesize[0];
                    $height = $imagesize[1];
                    $imagemaxwidth = get_config('imagemaxwidth');
                    $imagemaxheight = get_config('imagemaxheight');
                    if ($width > $imagemaxwidth || $height > $imagemaxheight) {
                        $error = get_string('profileiconimagetoobig', 'artefact.file', $width, $height, $imagemaxwidth, $imagemaxheight);
                    }

                    try {
                        $user->quota_add($filesize);
                    } catch (QuotaException $qe) {
                        $error = get_string('profileiconuploadexceedsquota', 'artefact.file', get_config('wwwroot'));
                    }

                    require_once(get_config('docroot') . '/artefact/lib.php');
                    require_once(get_config('docroot') . '/artefact/file/lib.php');

                    // Entry in artefact table
                    $artefact = new ArtefactTypeProfileIcon();
                    $artefact->set('owner', $user->id);
                    $artefact->set('title', 'Profile Icon');
                    $artefact->set('note', 'Profile Icon');
                    $artefact->set('size', $filesize);
                    $artefact->set('filetype', $mime);
                    $artefact->set('width', $width);
                    $artefact->set('height', $height);
                    $artefact->commit();

                    $id = $artefact->get('id');

                    // Move the file into the correct place.
                    $directory = get_config('dataroot') . 'artefact/file/profileicons/originals/' . ($id % 256) . '/';
                    check_dir_exists($directory);
                    rename($filename, $directory . $id);
                    if ($create || empty($icons)) {
                        $user->profileicon = $id;
                    }
                }

                $user->commit();
            } else {
                log_warn(get_string('cantcreatetempprofileiconfile', 'artefact.file', $filename));
            }
        }
        return empty($error);
    }

    /**
     * Given a user and their remote user record, attempt to set required fields,
     * namely firstname, lastname, email
     *
     * @param User  $user
     * @param array $rpxdata
     * @return array $imported imported fields
     */
    private function import_required_fields(&$user, $rpxdata) {
        $imported = array();
        // import firstname
        if (!empty($rpxdata['profile']['name']['givenName'])) {
            $user->firstname = $rpxdata['profile']['name']['givenName'];
            $imported[] = 'firstname';
        }
        // import lastname
        if (!empty($rpxdata['profile']['name']['familyName'])) {
            $user->lastname = $rpxdata['profile']['name']['familyName'];
            $imported[] = 'lastname';
        }
        // verified email is the most preferable. If no email is found in
        // the data, we will ask user to enter it after the login
        if (!empty($rpxdata['profile']['verifiedEmail'])) {
            $user->email = $rpxdata['profile']['verifiedEmail'];
            $imported[] = 'email';
        }
        elseif (!empty($rpxdata['profile']['email'])) {
            $user->email = $rpxdata['profile']['email'];
            $imported[] = 'email';
        }

        return $imported;
    }

    /**
     * Given a user and their remote user record, attempt to populate some of
     * the user's profile fields and account settings from the remote data.
     *
     * This does not change the first name, last name or e-mail fields, as these are
     * dealt with differently depending on whether we are creating the user
     * record or updating it.
     *
     * @param User  $user
     * @param array $rpxdata
     * @return array $imported imported fields
     */
    private function import_user_settings($user, $rpxdata) {
        $imported = array();

        // Preferred name setting
	$preferredname = '';
	if (!empty($rpxdata['profile']['name']['formatted'])) {
	    $preferredname = $rpxdata['profile']['name']['formatted'];
	}
	elseif (!empty($rpxdata['profile']['displayName'])) {
	    $preferredname = $rpxdata['profile']['displayName'];
	}
        if (!empty($preferredname)) {
            set_profile_field($user->id, 'preferredname', $preferredname);
            $imported[] = 'preferredname';
        }

        // Personal website
        if (!empty($rpxdata['profile']['url'])) {
            set_profile_field($user->id, 'personalwebsite', $rpxdata['profile']['url']);
            $imported[] = 'personalwebsite';
        }

	// Town
        if (!empty($rpxdata['profile']['address']['locality'])) {
            set_profile_field($user->id, 'town', $rpxdata['profile']['address']['locality']);
            $imported[] = 'town';
        }

        // City
        if (!empty($rpxdata['profile']['address']['region'])) {
            set_profile_field($user->id, 'city', $rpxdata['profile']['address']['region']);
            $imported[] = 'city';
        }

        // Country
        if (!empty($rpxdata['profile']['address']['country'])) {
            $validcountries = array_keys(getoptions_country());
            $newcountry = strtolower($rpxdata['profile']['address']['country']);
            if (in_array($newcountry, $validcountries)) {
                set_profile_field($user->id, 'country', $newcountry);
            }
            $imported[] = 'country';
        }

	// Address
        if (!empty($rpxdata['profile']['address']['formatted'])) {
            set_profile_field($user->id, 'address', $rpxdata['profile']['address']['formatted']);
            $imported[] = 'address';
        }

        return $imported;
    }

   /*
    * Generate login link
    * as suggested by Janrain widget guidelines <http://www.rpxnow.com/>
    *
    * @return string
    */
    public function get_rpx_login_url() {
        $login_url = $this->rpxrealmscheme . '://' . $this->rpxrealm . '/openid/v2/signin';
        $token_url = get_config('wwwroot') . 'auth/rpx/index.php';
        $url = '<a class="rpxnow" onclick="return false;" href="' . $login_url . '?token_url=' . $token_url . '">' . get_string('loginlinktitle', 'auth.rpx') . '</a>';
        return $url;
    }

   /*
    * Generate javascript required for login link
    * as suggested by Janrain widget guidelines <http://www.rpxnow.com/>
    *
    * @return string
    */
    public function get_rpx_javascript() {
        global $JS;

        $js = <<< EOF
var rpxJsHost = (("https:" == document.location.protocol) ? "https://" : "http://static.");
var rpx = document.createElement('script'); rpx.type = 'text/javascript';
rpx.src = rpxJsHost + 'rpxnow.com/js/lib/rpx.js';
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(rpx);
EOF;
        $JS->add_string_to_js($js);

        $lang = preg_replace('/\.\S+$/', '', get_config('lang'));
        $js = 'RPXNOW.overlay = true; RPXNOW.language_preference = \'' . $lang . '\'';
        $JS->add_string_to_js($js);
    }

    /*
    * Generate embedded frame
    * as suggested by Janrain widget guidelines <http://www.rpxnow.com/>
    *
    * @return string
    */
    public function get_rpx_iframe() {
        $embed_url = $this->rpxrealmscheme . '://' . $this->rpxrealm . '/openid/embed';
        $token_url = get_config('wwwroot') . 'auth/rpx/index.php';
        $iframe = '<iframe id="rpx-embedded-registration" src="'. $embed_url . '?token_url=' . $token_url . '" scrolling="no" frameBorder="no" allowtransparency="true" style="width:400px;height:240px;"></iframe>';
        return $iframe;
    }
}

/**
 * Plugin configuration class
 */
class PluginAuthRpx extends PluginAuth {

    private static $default_config = array(
        'rpxapikey'		=> '',
        'updateuserinfoonlogin' => 1,
        'rpxadminurl'           => '',
        'rpxrealm'              => '',
        'rpxrealmscheme'        => '',
        'widgetstyle'           => 'overlay',
        );

    public static function has_config() {
        return false;
    }

    public static function get_config_options() {
        return array();
    }

    public static function has_instance_config() {
        return true;
    }

    public static function is_usable() {
        $extenstions_ready = extension_loaded('openssl') && extension_loaded('curl');
        // Ensure that plugin is not already in use by another institution
        if ($extenstions_ready && !count_records('auth_instance', 'authname', 'rpx')) {
            return true;
        }
        return false;
    }

    public static function get_instance_config_options($institution, $instance = 0) {
        if ($instance > 0) {
            $default = get_record('auth_instance', 'id', $instance);
            if ($default == false) {
                throw new SystemException('Could not find data for auth instance ' . $instance);
            }
            $current_config = get_records_menu('auth_instance_config', 'instance', $instance, '', 'field, value');

            if ($current_config == false) {
                $current_config = array();
            }

            foreach (self::$default_config as $key => $value) {
                if (array_key_exists($key, $current_config)) {
                    self::$default_config[$key] = $current_config[$key];
                }
            }
        }
        else {
            $default = new stdClass();
            $default->instancename = '';
        }

        $elements = array(
            'instance' => array(
                'type'  => 'hidden',
                'value' => $instance,
            ),
            'institution' => array(
                'type'  => 'hidden',
                'value' => $institution,
            ),
            'authname' => array(
                'type'  => 'hidden',
                'value' => 'rpx',
            ),
            'rpxapikey' => array(
                'type'  => 'text',
                'title' => get_string('rpxapikey', 'auth.rpx'),
                'rules' => array(
                    'required' => true,
                ),
                'defaultvalue' => self::$default_config['rpxapikey'],
                'help' => true,
            ),
            'widgetstyle' => array(
                'type'    => 'select',
                'title'   => get_string('widgetstyle', 'auth.rpx'),
                'options' => array(
                    'overlay' => get_string('widgetstyleoverlay', 'auth.rpx'),
                    'embed'   => get_string('widgetstyleembedded', 'auth.rpx'),
                ),
                'rules'   => array(
                    'required' => true,
                ),
                'defaultvalue' => self::$default_config['widgetstyle'],
                'help'   => true
            ),
            'updateuserinfoonlogin' => array(
                'type'  => 'checkbox',
                'title' => get_string('updateuserinfoonlogin', 'auth.rpx'),
                'defaultvalue' => self::$default_config['updateuserinfoonlogin'],
                'help'  => true,
            ),
        );

        if ($instance > 0) {
            $elements['adminurl'] = array(
                'type'  => 'html',
                'title' => get_string('adminurltitle', 'auth.rpx'),
                'value' => get_string('adminurlcontent', 'auth.rpx', self::$default_config['rpxadminurl']),
            );
            $elements['setupurl'] = array(
                'type'  => 'html',
                'value' => get_string('setupurlcontent', 'auth.rpx', self::$default_config['rpxadminurl'].'/setup_providers#steps'),
                'description' => get_string('setupurldescr', 'auth.rpx'),
            );

        }
        return array(
            'elements' => $elements,
            'renderer' => 'table'
        );
    }


    public static function validate_config_options($values, $form) {
        // First check that plugin is not in use by another institution. Only
        // one institution at the time is permitted.
        $institution = get_record_sql("SELECT i.name, i.displayname
            FROM {auth_instance} ai JOIN {institution} i ON ai.institution = i.name
            WHERE ai.authname = 'rpx'");

        if (!empty($institution) && (($values['instance'] == 0 &&
            $institution->name == $values['institution']) || $institution->name != $values['institution'])) {
            $form->set_error('rpxapikey', get_string('erralreadyinuse', 'auth.rpx', $institution->displayname));
            return;
        }
        // Now validate the api key
        $rpx = new RPX;
        try {
            $lookup = $rpx->lookup(trim($values['rpxapikey']));
        }
        catch (SystemException $e) {
            $form->set_error('rpxapikey', get_string('errcurl', 'auth.rpx'));
            return;
        }

        if ($lookup === null) {
            $form->set_error('rpxapikey', get_string('errrpxapikey', 'auth.rpx'));
            return;
        }
        // Store config variables
        self::$default_config = array('rpxapikey'             => $values['rpxapikey'],
                                      'widgetstyle'           => $values['widgetstyle'],
                                      'updateuserinfoonlogin' => $values['updateuserinfoonlogin'],
                                      'rpxadminurl'           => $lookup['adminUrl'],
                                      'rpxrealm'              => $lookup['realm'],
                                      'rpxrealmscheme'        => $lookup['realmScheme'],
                                     );
    }


    public static function save_config_options($values, $form) {

        $authinstance = new stdClass();

        if ($values['instance'] > 0) {
            $values['create'] = false;
            $current = get_records_assoc('auth_instance_config', 'instance', $values['instance'], '', 'field, value');
            $authinstance->id = $values['instance'];
        }
        else {
            $values['create'] = true;
            $lastinstance = get_records_array('auth_instance', 'institution', $values['institution'], 'priority DESC', '*', '0', '1');

            if ($lastinstance == false) {
                $authinstance->priority = 0;
            }
            else {
                $authinstance->priority = $lastinstance[0]->priority + 1;
            }
        }

        $authinstance->institution = $values['institution'];
        $authinstance->authname = $values['authname'];
        $authinstance->instancename = get_string('title', 'auth.rpx');

        if ($values['create']) {
            $values['instance'] = insert_record('auth_instance', $authinstance, 'id', true);
            $values['instancename'] = get_string('title', 'auth.rpx');
        }
        else {
            update_record('auth_instance', $authinstance, array('id' => $values['instance']));
        }

        if (empty($current)) {
            $current = array();
        }

        foreach (self::$default_config as $field => $value) {
            $record = new stdClass();
            $record->instance = $values['instance'];
            $record->field = $field;
            $record->value = trim($value);

            if ($values['create'] || !array_key_exists($field, $current)) {
                insert_record('auth_instance_config', $record);
            }
            else {
                update_record('auth_instance_config', $record, array('instance' => $values['instance'], 'field' => $field));
            }
        }
        return $values;
    }

    public function pieform_login_elementhook($elements) {
        $instance = get_record_sql("SELECT ai.*, i.*
            FROM {auth_instance} ai JOIN {institution} i ON ai.institution = i.name
            WHERE ai.authname = 'rpx' AND i.suspended = 0");
        // No auth instance - no login form mangling
        if (empty($instance)) {
            return $elements;
        }

        $auth = new AuthRpx($instance->id);

        if ($auth->__get('widgetstyle') == 'overlay') {
            // add javascript to header
            $auth->get_rpx_javascript();
            // add the link to login form
            $elements['login_rpxlink'] = array(
                'type'  => 'html',
                'value' => '<div style="margin-top: 0.5em; text-align: center;">' . $auth->get_rpx_login_url() . '</div>',
            );
        }
        else {
            // embed widget
            $elements['login_rpxiframe'] = array(
                'value' => $auth->get_rpx_iframe(),
            );
        }

        return $elements;
    }
}

?>