<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-rpx
 * @author     Ruslan Kabalin <ruslan.kabalin@luns.net.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2010 Lancaster University Network Services Limited
 *                      http://www.luns.net.uk
 */

define('INTERNAL', 1);
define('PUBLIC', 1);

require(dirname(dirname(dirname(__FILE__))) . '/init.php');

require_once(get_config('docroot') . 'auth/rpx/lib.php');
require_once(get_config('libroot') .'institution.php');

$token = param_variable('token');

// RPX plugin can only be used with one institution at the time,
// so we expect only one record
$instance = get_record_sql("SELECT ai.*, i.*
    FROM {auth_instance} ai JOIN {institution} i ON ai.institution = i.name
    WHERE ai.authname = 'rpx'");

// RPX is not used by any institution
if (empty($instance)) {
    log_warn("auth/rpx: could not find an authinstance for rpx");
    throw new UserNotFoundException(get_string('errnoauthinstance', 'auth.rpx'));
}

// Ensure desitnation institutuion is not suspended
if ($instance->suspended) {
    $sitename = get_config('sitename');
    throw new AccessTotallyDeniedException(get_string('accesstotallydenied_institutionsuspended', 'mahara', $instance->displayname, $sitename));
}

// if the user is not logged in, then process login
if (!$USER->is_logged_in()) {
    $auth = new AuthRpx($instance->id);
    if ($auth->request_user_authorise($token)) {
        redirect($CFG->wwwroot);
    }
    else {
        throw new UserNotFoundException(get_string('errnorpxuser', 'auth.rpx'));
    }
}
// they are logged in, they don't need to be here
else {
    redirect($CFG->wwwroot);
}
?>
