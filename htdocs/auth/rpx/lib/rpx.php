<?php

/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-rpx
 * @author     Ruslan Kabalin <ruslan.kabalin@luns.net.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2010 Lancaster University Network Services Limited
 *                      http://www.luns.net.uk
 *
 * This file incorporates work covered by the following copyright and
 * permission notice:
 *
 *    Drupal RPX Module <http://drupal.org/project/rpx>
 *    Authors:	Peat Bakke <peat@janrain.com>, Nathan Rambeck <http://nathan.rambeck.org>,
 * 		Rendahl Weishar <ren@janrain.com>
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details:
 *
 *             http://www.gnu.org/copyleft/gpl.html
 */

defined('INTERNAL') || die();

class RPX {

    private $lookup_api = 'https://rpxnow.com/plugin/lookup_rp';
    private $auth_api = 'https://rpxnow.com/api/v2/auth_info';

    /* performs a lookup request for getting information about an RPX account */
    public function lookup($api_key) {
        $raw_result = $this->http_post($this->lookup_api, array('apiKey' => $api_key));

        if (!$raw_result) {
            return false;
        }

        return json_decode($raw_result, true);
    }

    /* fetches authorization information from a token */
    public function auth_info($token, $api_key) {
        $post_data = array(
            'token' => $token,
            'apiKey' => $api_key,
            'format' => 'json'
        );

        $raw_result = $this->http_post($this->auth_api, $post_data);
        return json_decode($raw_result, true);
    }


    /* HTTP POST request */
    private function http_post($url, $post_data) {
        $request = array(
		CURLOPT_URL        => $url,
		CURLOPT_POST       => true,
		CURLOPT_POSTFIELDS => $post_data,
	        CURLOPT_TIMEOUT    => 15,
        );

        $result = mahara_http_request($request);
        if ($result->error) {
	    throw new SystemException($result->error);
        }
	return $result->data;
    }

}

?>